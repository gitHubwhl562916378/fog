#include "mipmaprender.h"
#include <QDebug>

MipMapRender::~MipMapRender()
{
    texture_->destroy();
    delete texture_;
}

void MipMapRender::initsize()
{
    program_.addCacheableShaderFromSourceFile(QOpenGLShader::Vertex,"vsrc.vert");
    program_.addCacheableShaderFromSourceFile(QOpenGLShader::Fragment,"fsrc.frag");
    program_.link();

    texture_ = new QOpenGLTexture(QOpenGLTexture::Target2D);
    GLfloat points[4 * 3 + 8]{
        -1.0,-1.0,0.0,
        +1.0,-1.0,0.0,
        +1.0,+1.0,0.0,
        -1.0,+1.0,0.0,

        0.0,10.0,
        10.0,10.0,
        10.0,0.0,
        0.0,0.0
    };
    vbo_.create();
    vbo_.bind();
    vbo_.allocate(points, sizeof points);
}

void MipMapRender::render(QOpenGLExtraFunctions *f, QMatrix4x4 &pMatrix, QMatrix4x4 &vMatrix, QMatrix4x4 &mMatrix, QVector3D &cameraLocation, QImage &img, bool miped)
{
    f->glEnable(GL_DEPTH_TEST);
    f->glEnable(GL_CULL_FACE);

    program_.bind();
    vbo_.bind();
    f->glActiveTexture(GL_TEXTURE0 + 0);
    program_.setUniformValue("sTexture",0);
    program_.setUniformValue("uPMatrix",pMatrix);
    program_.setUniformValue("uVMatrix",vMatrix);
    program_.setUniformValue("uMMatrix",mMatrix);
    program_.setUniformValue("uCamera",cameraLocation);
    program_.enableAttributeArray(0);
    program_.enableAttributeArray(1);

    program_.setAttributeBuffer(0,GL_FLOAT,0,3,3 * sizeof GLfloat);
    program_.setAttributeBuffer(1,GL_FLOAT,4 * 3 * sizeof GLfloat, 2, 2 * sizeof GLfloat);

    texture_->destroy();
    if(miped){
        texture_->setData(img);
        texture_->setMinificationFilter(QOpenGLTexture::LinearMipMapNearest);
        texture_->setMagnificationFilter(QOpenGLTexture::LinearMipMapLinear);
        texture_->setWrapMode(QOpenGLTexture::DirectionS,QOpenGLTexture::Repeat);
        texture_->setWrapMode(QOpenGLTexture::DirectionT,QOpenGLTexture::Repeat);
    }else{
        texture_->setData(img,QOpenGLTexture::DontGenerateMipMaps);
        texture_->setMinificationFilter(QOpenGLTexture::Nearest);
        texture_->setMagnificationFilter(QOpenGLTexture::Linear);
        texture_->setWrapMode(QOpenGLTexture::DirectionS,QOpenGLTexture::Repeat);
        texture_->setWrapMode(QOpenGLTexture::DirectionT,QOpenGLTexture::Repeat);
    }
    qDebug() << texture_->mipLevels() << texture_->mipBaseLevel() << texture_->maximumMipLevels();
    texture_->bind(0);
    f->glDrawArrays(GL_TRIANGLE_FAN,0,4);

    program_.disableAttributeArray(0);
    program_.disableAttributeArray(1);
    texture_->release();
    vbo_.release();
    program_.release();

    f->glDisable(GL_DEPTH_TEST);
    f->glDisable(GL_CULL_FACE);
}
