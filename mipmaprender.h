#ifndef MIPMAPRENDER_H
#define MIPMAPRENDER_H

#include <QOpenGLExtraFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>
class MipMapRender
{
public:
    MipMapRender() = default;
    ~MipMapRender();
    void initsize();
    void render(QOpenGLExtraFunctions *f,QMatrix4x4 &pMatrix,QMatrix4x4 &vMatrix,QMatrix4x4 &mMatrix,QVector3D &cameraLocation,QImage &img,bool miped);

private:
    QOpenGLShaderProgram program_;
    QOpenGLBuffer vbo_;
    QOpenGLTexture *texture_{nullptr};
};

#endif // MIPMAPRENDER_H
