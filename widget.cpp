#include "widget.h"

Widget::Widget(QWidget *parent)
    : QOpenGLWidget(parent)
{
}

Widget::~Widget()
{
    makeCurrent();
}

void Widget::initializeGL()
{
    render_.initsize();
    dlRender_.initsize("ch.obj");
    imageRender_.initsize("lgq.png");
    imageRender1_.initsize("lgq1.png");
    lightLocation_.setX(10);
    lightLocation_.setY(10);
    lightLocation_.setZ(0);
    camera_.setX(0.0);
    camera_.setY(3.5);
    camera_.setZ(30.0);
}

void Widget::resizeGL(int w, int h)
{
    pMatrix.setToIdentity();
    pMatrix.perspective(30,float(w) / h,0.01f,100.0f);
}

void Widget::paintGL()
{
    QOpenGLExtraFunctions *f = QOpenGLContext::currentContext()->extraFunctions();
    f->glClearColor(0.0f,0.0f,0.0f,1.0f);
    f->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    QMatrix4x4 vMatrix;
    vMatrix.lookAt(camera_,QVector3D{0.0f,3.5f,0.0f},QVector3D{0.0f,1.0f,0.0f});

    QMatrix4x4 mMatrix;
    mMatrix.scale(7,3.5,18);
    mMatrix.rotate(-90,1,0,0);
    render_.render(f,pMatrix,vMatrix,mMatrix,camera_,QImage("floor.jpg"),true);

    mMatrix.setToIdentity();
    mMatrix.translate(-7.0,3.5,0.0);
    mMatrix.scale(7,3.5,18);
    mMatrix.rotate(90,0,1,0);
    render_.render(f,pMatrix,vMatrix,mMatrix,camera_,QImage("wall.jpg"),true);

    mMatrix.setToIdentity();
    mMatrix.translate(7.0,3.5,0.0);
    mMatrix.scale(7,3.5,18);
    mMatrix.rotate(-90,0,1,0);
    render_.render(f,pMatrix,vMatrix,mMatrix,camera_,QImage("wall.jpg"),true);

    mMatrix.setToIdentity();
    mMatrix.translate(0.0,7.0,0.0);
    mMatrix.scale(7,3.5,18);
    mMatrix.rotate(90,1,0,0);
    render_.render(f,pMatrix,vMatrix,mMatrix,camera_,QImage("sky.jpg"),true);

    mMatrix.setToIdentity();
    mMatrix.translate(-1,1.8,0.0);
    mMatrix.scale(0.3);
    mMatrix.rotate(45,1,0,0);
    dlRender_.render(f,pMatrix,vMatrix,mMatrix,camera_,lightLocation_);

    mMatrix.setToIdentity();
    mMatrix.translate(3,1,10);
    imageRender_.render(f,pMatrix,vMatrix,mMatrix,camera_);

    f->glEnable(GL_BLEND);
    mMatrix.setToIdentity();
    mMatrix.translate(-1.2,1.6,10);
    mMatrix.scale(1.2);
    f->glBlendFunc(GL_SRC_COLOR,GL_ONE_MINUS_SRC_COLOR);
    imageRender_.render(f,pMatrix,vMatrix,mMatrix,camera_);

    mMatrix.setToIdentity();
    mMatrix.translate(1.0,2.6,10);
    f->glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    imageRender1_.render(f,pMatrix,vMatrix,mMatrix,camera_);
    f->glDisable(GL_BLEND);
}
