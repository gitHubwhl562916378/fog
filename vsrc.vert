#version 330
uniform mat4 uPMatrix,uVMatrix,uMMatrix;
uniform vec3 uCamera;
layout (location = 0) in vec3 aPosition;
layout (location = 1) in vec2 aTexture;
smooth out vec2 vTextureCood;
smooth out float vFogFactor;

float computeFogFactor(){
   float tmpFactor;
   float fogDistance = length(uCamera-(uMMatrix*vec4(aPosition,1)).xyz);//顶点到摄像机的距离
   const float end = 5;//雾结束位置
   const float start = 30;//雾开始位置
//   tmpFactor = max(min((end- fogDistance)/(end-start),1.0),0.0);//用雾公式计算雾因子,线型
   tmpFactor = 1.0 - smoothstep(start,end,fogDistance);//用雾公式计算雾因子,非线型
   return tmpFactor;
}

void main(void)
{
    gl_Position = uPMatrix * uVMatrix * uMMatrix * vec4(aPosition,1);
    vTextureCood = aTexture;
    vFogFactor = computeFogFactor();
}
