#version 330
uniform sampler2D sTexture;
in vec2 vTextureCood;
in float vFogFactor;
out vec4 fragColor;

void main(void)
{
    vec4 objColor = texture2D(sTexture,vTextureCood);
    vec4 fogColor = vec4(0.97,0.76,0.03,1.0);//雾的颜色
    if(vFogFactor != 0.0){
        fragColor = objColor*vFogFactor + fogColor*(1.0-vFogFactor);
    }else{
        fragColor=fogColor;
    }
}
