#ifndef DOUBLELIGHTOBJRENDER_H
#define DOUBLELIGHTOBJRENDER_H

#include <QOpenGLShaderProgram>
#include <QOpenGLExtraFunctions>
#include <QOpenGLTexture>
#include <QOpenGLBuffer>
class DoubleLightObjRender
{
public:
    DoubleLightObjRender() = default;
    void initsize(QString filename);
    void render(QOpenGLExtraFunctions *f,QMatrix4x4 &pMatrix,QMatrix4x4 &vMatrix,QMatrix4x4 &mMatrix,QVector3D &cameraLocation,QVector3D &lightCation);

private:
    QOpenGLBuffer vbo_;
    QOpenGLShaderProgram program_;
    QVector<float> vertPoints_,normalPoints_;
};

#endif // DOUBLELIGHTOBJRENDER_H
